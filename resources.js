var mongo = require('mongodb');
var eventEmitter = require('events').EventEmitter;


var mongoserver = mongo.Server;
var mongodb = mongo.Db;

//var db = new mongodb('ruvonumvo', new mongoserver('127.0.0.1', 27017, {auto_reconnect: true, safe: true}));

//mongodb://nodejitsu:ab64fa880ae4ad3d7ec5a2e6c32184f4@alex.mongohq.com:10053/nodejitsudb7594257502

var host = 'alex.mongohq.com';
var port = 10053;
var username = 'nodejitsu';
var password = 'ab64fa880ae4ad3d7ec5a2e6c32184f4';
var database = 'nodejitsudb7594257502';

var db = new mongodb(database, new mongoserver(host, port, {auto_reconnect: true, safe:true}));

db.open(
    function(err, db){
        db.authenticate(username, password, function(err, res){
            if(err){
                console.log('Database Error!', err);

                events.emit('request-error', {
                    code: 500
                })
            }  else{
                console.log('Successfully connected to database on host', host);
            }
        });
    }
);






var resources = {};

var events = new eventEmitter;

function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
};

function guid() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}


resources.favoritos = {
    "post": function (params) {

    },

    "get": function (params) {

    },

    "delete": function (params) {

    },

    "put": function (params) {

    }
};

resources.prato = {
    "post": function (params) {
        var pratos = db.collection('pratos');

        pratos.insert({
            "nome": params['nome'],
            "data": params['data'],
            "prato_id": guid(),
            "ratings": 0,
            "n_ratings": 1
        }, {w: 1}, function (err, result) {
            if (!err)
                events.emit('request-done', result, 201);
            else
                events.emit('request-error', {
                    code: 504
                })
        });
    },
    "get": function (params) {
        var pratos = db.collection('pratos');

        if (params['id'] !== undefined) {

            var search = {
                "prato_id": params['id']
            }

            pratos.find(search).toArray(function (err, items) {
                if (!err) {
                    events.emit('request-done', items, 200);
                } else {
                    events.emit('request-error', {
                        code: 500
                    })
                }
            });
        } else {

            var search = {};

            if(params['data'] !== undefined){
                search['data'] = params['data'];
            }

            pratos.find(search).toArray(function (err, items) {
                if (!err)
                    events.emit('request-done', items, 200);
                else
                    events.emit('request-error', {
                        code: 500
                    })
            });
        }

    },
    "delete": function (params) {

    },
    "put": function (params) {

    }
};

resources.usuario = {
    "post": function (params) {
        var usuarios = db.collection('usuarios');

        usuarios.insert({
            "nome": params['nome'],
            "email": params['email'],
            "senha": params['senha'],
            "universidade": params['universidade'],
            "user_id": guid()

        }, {w: 1}, function (err, result) {
            if (!err)
                events.emit('request-done', result, 201);
            else
                events.emit('request-error', {
                    code: 504
                })
        });
    },
    "get": function (params) {
        var usuarios = db.collection('usuarios');

        if (params['id'] !== undefined) {

            var search = {
                "user_id": params['id']
            }

            usuarios.find(search).toArray(function (err, items) {
                if (!err) {
                    events.emit('request-done', items, 200);
                } else {
                    events.emit('request-error', {
                        code: 500
                    })
                }
            });
        } else {
            usuarios.find().toArray(function (err, items) {
                if (!err)
                    events.emit('request-done', items, 200);
                else
                    events.emit('request-error', {
                        code: 500
                    })
            });
        }
    },
    "put": function () {

    }
};

resources.rating = {
    "put": function (params) {
        var pratos = db.collection('pratos');

        pratos.update({
            "prato_id": params['id']
        }, {
            $inc: {
                "ratings": parseInt(params['rating']),
                "n_ratings": 1
            }
        }, {}, function (err, item) {
            if (!err)
                events.emit('request-done', item, 200);
            else
                events.emit('request-error', {
                    code: 500
                })
        });

    }
}

resources.favorito = {
    "post": function (params) {
        var favoritos = db.collection('favoritos');

        favoritos.insert({
            "user_id": params['id'],
            "prato_id": params['prato_id']
        }, {w: 1}, function (err, item) {
            if (!err) {
                events.emit('request-done', item, 200);
            } else {
                events.emit('request-error', {
                    code: 500
                })
            }
        });
    },
    "get": function (params) {
        var favoritos = db.collection('favoritos');

        favoritos.find({
            "user_id": params['id']
        }).toArray(function (err, items) {
                if (!err) {
                    var pratos = db.collection('pratos');

                    var or = [];

                    for (var i in items) {
                        or.push({
                            "prato_id": items[i].prato_id
                        })
                    }

                    pratos.find({
                        $or: or
                    }).toArray(function (err, pratos) {
                            if (!err) {
                                events.emit('request-done', pratos, 200);
                            } else {
                                events.emit('request-error', {
                                    code: 500
                                })
                            }
                        });


                } else {
                    events.emit('request-error', {
                        code: 500
                    })
                }

            });
    }
}

/**
 *
 * @param resource
 * @param method
 * @param params
 * @returns {*}
 */
exports.process = function (resource, method, params) {
    method = method.toLowerCase();


    if (resources[resource] === undefined) {
        events.emit('request-error', {
            code: 404
        })

        return;
    } else if (resources[resource][method] === undefined) {
        events.emit('request-error', {
            code: 405
        })

        return;
    } else {
        try {
            resources[resource][method](params);
        } catch (Error) {
            console.log('Internal Error', Error);

            events.emit('request-error', {
                code: 500
            })
        }
    }


};

exports.events = events;
