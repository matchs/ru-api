var server = require('./server.json');
var http = require('http');
var resources = require('./resources');
var url = require('url');


http.createServer(function (request, response) {

    var rurl = request.url.split('/');
    var step2 = rurl.toString();
    var rurl = step2.split('?');

    rurl.splice(1,1);

    var step2 = rurl.toString();

    var params = step2.split(',');

    var queryObj = url.parse(request.url, true).query;
    params.splice(0,1);

    var resource = params[0];

    if(params[1]!== undefined )
        queryObj['id']=params[1];


    console.log('Request received with params:', queryObj);
    //try {

    /*} catch (Error) {

        response.writeHead(500, 'Internal Server Error', server["headers"]);
        response.end();
    } */

    resources.events.on('request-done',function(resp,statusCode){
        console.log('Request responde with', statusCode);

        response.writeHead(statusCode, 'Done', server["headers"]);
        response.end(JSON.stringify(resp));
    });

    resources.events.on('request-error', function(resp){
        var errors = {
            404:'Resource not found',
            405: 'Method not allowed',
            500: 'Internal server error'
        }

        console.log('Request responded with', resp.code);

        response.writeHead(resp.code, errors[resp.code], server["headers"]);
        response.end();
    });


    resources.process(resource, request.method, queryObj);

}).listen(8080);

